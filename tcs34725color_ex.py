#!/usr/bin/python3
#
# tcs34725color_ex.py
#
# Simple demo of the TCS34725 color sensor.
# Will detect the color temperature and light
# intensity (lux) from the sensor and print it 
# out every second.
#
# from: 
# https://github.com/adafruit/Adafruit_CircuitPython_TCS34725/blob/master/examples/tcs34725_simpletest.py
#
# Seth McNeill
# 2020 Janaury 23

import time
import board
import busio

import adafruit_tcs34725


# Initialize I2C bus and sensor.
i2c = busio.I2C(board.SCL, board.SDA)
sensor = adafruit_tcs34725.TCS34725(i2c)

# Main loop reading color and printing it every second.
while True:
    # Read the color temperature and lux of the sensor too.
    temp = sensor.color_temperature
    lux = sensor.lux
    print('Temperature: {0}K Lux: {1}'.format(temp, lux))
    # Delay for a second and repeat.
    time.sleep(1.0)

# other available functions:
# the color_rgb_bytes doesn't seem too great...
# probably measuring the intensity (lux) and color_temperature
# like this script does is best for this sensor. Let me know if you
# find differently.
#
# sensor.active             sensor.cycles             sensor.lux
# sensor.color              sensor.gain               sensor.max_value
# sensor.color_raw          sensor.glass_attenuation  sensor.min_value
# sensor.color_rgb_bytes    sensor.integration_time
# sensor.color_temperature  sensor.interrupt
