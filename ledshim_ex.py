#!/usr/bin/python3
# 
# ledshim_ex.py
#
# Example of controlling the ledshim.
#
# Seth McNeill
# 2020 January 21

import ledshim
import random
import sys
from time import sleep

lenled = ledshim.get_shape()[0]

cnt = 0
N = 10
while(cnt < N):
  for ii in range(0,lenled):
    ledshim.set_pixel(ii,random.randint(0,255), random.randint(0,255), random.randint(0,255))
  ledshim.show()
  sleep(2)
  cnt += 1


